package com.example.proyecto1.ViewModels;

import java.util.List;

public class Peticion_Usuarios {
    public String estado;
    public List<Usuarios> usuarios;
    public String detalle;


    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<Usuarios> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuarios> usuarios) {
        this.usuarios = usuarios;
    }


}

