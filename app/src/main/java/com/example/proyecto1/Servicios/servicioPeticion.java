package com.example.proyecto1.Servicios;


import com.example.proyecto1.ViewModels.Detalle_Usuario;
import com.example.proyecto1.ViewModels.Peticion_Login;
import com.example.proyecto1.ViewModels.Peticion_Usuarios;
import com.example.proyecto1.ViewModels.Registro_Usuario;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface servicioPeticion {
    @FormUrlEncoded
    @POST("api/crearUsuario")
    Call<Registro_Usuario>registrarUsuario(@Field("username") String correo, @Field("password") String contrasenia);

    @FormUrlEncoded
    @POST("api/loginSocial")
    Call<Peticion_Login> login(@Field("username") String correo, @Field("password") String contrasenia);

    @POST("api/todosUsuarios")
    Call<Peticion_Usuarios> getUsuarios();

    @FormUrlEncoded
    @POST("api/detallesUsuario")
    Call<Detalle_Usuario> detallesUsuario(@Field("usuarioId") int usuarioId);


}
