package com.example.proyecto1.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.proyecto1.Api.Api;
import com.example.proyecto1.R;
import com.example.proyecto1.Servicios.servicioPeticion;
import com.example.proyecto1.ViewModels.Detalle_Usuario;
import com.example.proyecto1.ViewModels.Peticion_Login;
import com.example.proyecto1.ViewModels.Peticion_Usuarios;

import java.lang.reflect.Array;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Menu extends AppCompatActivity {

    private TextView cerrarsesion;
    private TextView Usuario;
    private ListView lista;
    private String APITOKEN;
    private String usuario;
    ArrayList<String> listaNombres=new ArrayList<>();
    ArrayList<String> listaCreacion=new ArrayList<>();
    ArrayList<String> listaActualizacion=new ArrayList<>();
    final ArrayList<Integer> IdUsuarios=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        getSupportActionBar().hide();
        cerrarsesion=(TextView) findViewById(R.id.txtv1menu);
        Usuario=(TextView) findViewById(R.id.txtv1menu2);
        lista=(ListView) findViewById(R.id.ltvMenu);
        SharedPreferences preferencias= getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        final String Usuariotxt=preferencias.getString("Usuario","");
        Usuario.setText("Conectado: "+Usuariotxt);
        Usuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Menu.this, "Hola "+ Usuariotxt, Toast.LENGTH_LONG).show();
            }
        });
        servicioPeticion service = Api.getApi(Menu.this).create(servicioPeticion.class);
        Call<Peticion_Usuarios> peticion_usuariosCall= service .getUsuarios();
        peticion_usuariosCall.enqueue(new Callback<Peticion_Usuarios>() {
            @Override
            public void onResponse(Call<Peticion_Usuarios> call, Response<Peticion_Usuarios> response) {
                final Peticion_Usuarios peticion=response.body();
                if(response.body()==null){
                    Toast.makeText(Menu.this,"Ocurrio un error, intentalo más tarde",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(peticion.estado == "true"){
                    for(int i=0;i<peticion.usuarios.size()-1;i++){
                        listaNombres.add(peticion.usuarios.get(i).username);
                        listaCreacion.add(peticion.usuarios.get(i).created_at);
                        listaActualizacion.add(peticion.usuarios.get(i).updated_at);
                        IdUsuarios.add(peticion.usuarios.get(i).id);
                    }
                    ArrayAdapter adapter=new ArrayAdapter(Menu.this,android.R.layout.simple_list_item_1,listaNombres);
                    lista.setAdapter(adapter);
                }else{
                    Toast.makeText(Menu.this,peticion.detalle,Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<Peticion_Usuarios> call, Throwable t) {
                Toast.makeText(Menu.this,"No podemos conectarnos :c",Toast.LENGTH_SHORT).show();
            }
        });

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                obtenerinformacion(IdUsuarios.get(position),listaCreacion.get(position),listaActualizacion.get(position));
            }
        });



        cerrarsesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                APITOKEN="";
                usuario="";
                guardarPreferencias();
                startActivity(new Intent(Menu.this,Login.class));
            }
        });

    }
    private void obtenerinformacion(final int id, final String Creacion, final String Actualizacion){
        servicioPeticion service = Api.getApi(Menu.this).create(servicioPeticion.class);
        Call<Detalle_Usuario> detalleUsuarioCall= service .detallesUsuario(id);
        detalleUsuarioCall.enqueue(new Callback<Detalle_Usuario>() {
            @Override
            public void onResponse(Call<Detalle_Usuario> call, Response<Detalle_Usuario> response) {
                Detalle_Usuario peticion=response.body();
                if(response.body()==null){
                    Toast.makeText(Menu.this,"Ocurrio un error, intentalo más tarde",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(peticion.estado == "true"){
                    AlertDialog.Builder builder = new AlertDialog.Builder(Menu.this);
                    builder.setTitle("Nombre de usuario: " +peticion.usuario);
                    builder.setMessage("Su contraseña esta encriptada no intentes nada :) : "+peticion.password +"\n" +
                            "El usuario se unio: "+Creacion+"\n Su ultima actualizacion fue: "+Actualizacion);
                    builder.setPositiveButton("Aceptar", null);

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }else{
                    Toast.makeText(Menu.this,peticion.estado,Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<Detalle_Usuario> call, Throwable t) {
                Toast.makeText(Menu.this,"No podemos conectarnos :c",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void guardarPreferencias(){
        SharedPreferences preferences = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor= preferences.edit();
        editor.putString("Token",APITOKEN);
        editor.putString("Usuario",usuario);
        editor.commit();
    }
}
